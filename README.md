mongo-news-restapi

- clone this repo and do :
    npm install
- to run this app locally, do:
    node app.js OR nodemon app.js


Unit Testing :
- not finished yet because of some trouble in windows env

List of TOPIC api :

- list (GET): http://localhost:3000/api/kumparan/v1/topics/list
- create new topic (POST): http://localhost:3000/api/kumparan/v1/topics/create
        body:
              {
                tag: "{tag}" //i.e "olahraga"
              }
- remove topic (DELETE): http://localhost:3000/api/kumparan/v1/topics/delete/:topicsId

Seed of topicsId :
- 5b02b093d7f1360a80b14b37 : budaya
- 5b02b09cd7f1360a80b14b38 : bola
- 5b02b0a1d7f1360a80b14b39 : pendidikan
- 5b02b0abd7f1360a80b14b3a : kesehatan
- 5b02be3a029f561f1c141030 : olahraga

List of NEWS api :

- list (GET) : http://localhost:3000/api/kumparan/v1/news/list
- create news (POST) : http://localhost:3000/api/kumparan/v1/news/create
        body:
              {
                title: "{title}",
                description: "{description}",
                status: "{status}",
                topic: "[idTopic1, idTopic2]"
              }
- remove news (DELETE): http://localhost:3000/api/kumparan/v1/news/delete/:newsId
        req.params.newsId from id each news object
- filter news (GET): http://localhost:3000/api/kumparan/v1/news/filter-status?status=status1,status2
        req.query.status = "status1,status2"
        status1 i.e = "draft"
        status2 i.e = "publish"
- filter topic (GET): http://localhost:3000/api/kumparan/v1/news/filter-topic?topic=topicId1,topicId2
        req.query.topic = "topicId1,topicId2"
        topicId1 i.e = "5b02b0abd7f1360a80b14b3a"
        topicId2 i.e = "5b02be3a029f561f1c141030"
