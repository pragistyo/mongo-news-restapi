const express = require('express');
const router = express.Router();
const topicsController = require('../controllers/topics')

router.get('/list', topicsController. getAll)
router.post('/create', topicsController.create)
router.post('/filter', topicsController.filter)
router.put('/update/:topicsId', topicsController.update)
router.delete('/delete/:topicsId', topicsController.remove)


module.exports = router;
