const express = require('express');
const router = express.Router();
const newsController = require('../controllers/news')

router.get('/list', newsController. getAll)
router.post('/create', newsController.create)
router.get('/filter-status', newsController.filterStatus)
router.get('/filter-topic', newsController.filterTopic)
router.put('/update/:newsId', newsController.update)
router.delete('/delete/:newsId', newsController.remove)

module.exports = router;
