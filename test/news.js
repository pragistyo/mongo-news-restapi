const const chai     = require('chai');
const should   = chai.should();
const chaiHttp = require('chai-http');
const app      = require('../app');
const basepath = '/api/kumparan/v1';

chai.use(chaiHttp)

const newsTest = {
  title: "Jogging Pagi",
  description: "Olahraga yang mudah namun membuat kita.....",
  status: "deleted",
  topic:["5b02b0abd7f1360a80b14b3a", "5b02be3a029f561f1c141030"]
  //kesehatan: 5b02b0abd7f1360a80b14b3a
  //olahraga: 5b02be3a029f561f1c141030
}

describe('test post news', ()=>{
  it('should be post news',  (done)=>{
    chai.request(app)
    .get(basepath+'/news/create')
    .send(news)
    .end((err, response)=> {
        console.log(response)
        response.status.should.be.equal(201)
        response.body.should.be.an('array')
        done()
    })
  })
})
