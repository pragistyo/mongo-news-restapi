const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var topicSchema = new Schema({
  tag: {type: String},
  news: [{type:Schema.Types.ObjectId, ref: 'News'}]
})

var topics = mongoose.model('Topics', topicSchema);
module.exports = topics;
