const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var newsSchema = new Schema({
  title: {type:String},
  description: {type:String},
  topic: [{type:Schema.Types.ObjectId, ref: 'Topics'}],
  status: {type:String}
  //img: { data: Buffer, contentType: String }
})

var news = mongoose.model('News', newsSchema);
module.exports = news;
