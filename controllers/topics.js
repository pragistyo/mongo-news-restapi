const TopicsModels = require('../models/Topics')
const mongoose = require('mongoose')

class Topics {
  constructor(){

  }

  static getAll(req,res,next){
    TopicsModels.find()
    .then( result =>{
      console.log('GET ALL: ', result)
      res.status(200).json(result)
    }).catch(err =>{
      console.log('ERROR GET ALL: ', err)
      res.status(500).send(err)
    })
  }

  static create(req,res,next){
    console.log('INI TAG: ', req.body.tag)
    TopicsModels.create({
      tag: req.body.tag
    })
    .then( result => {
      console.log('CREATE TOPICS: ', result)
      res.status(201).send('succes create new topic: '+ req.body.tag)
    }).catch(err => {
      console.log('ERROR CREATE TOPICS: ', err)
      res.status(400).send('ERROR CREATE TOPICS')
    })
  }

  static filter(req,res,next){
    //use $in
  }

  static update(req,res,next){
    TopicsModels.findOneAndUpdate(
      {_id:req.params.topicsId},
      {
        $addToSet:{news:req.body.news}
      },
      {new:true} // return updated object
    )
    .then( result => {
      console.log('RESULT OF UPDATED TOPIC: ', result)
      res.status(200).json(result)
    }).catch( err => {
      console.log('ERROR UPDATE TOPICS: ', err)
      res.status(403).send(`Fail to update topics data with Id ${req.params.topicsId}`)
    })
  }

  static remove(req,res,next){
    TopicsModels.findOneAndRemove({_id:req.params.topicsId})
    .then( result => {
      console.log('DELETE RESULT SUCCESS: ', result)
      res.status(200).send(`data with Id ${req.params.topicsId} has been deleted`)
    }).catch( err => {
      console.log('ERROR DELETE TOPICS TAG: ', err)
      res.status(403).send(`Fail to delete data with Id ${req.params.topicsId}`)
    })
  }
}

module.exports = Topics
