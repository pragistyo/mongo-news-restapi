const NewsModels = require('../models/News')
const mongoose = require('mongoose')

class News {
  constructor(){

  }

  static getAll(req,res,next){
    NewsModels.find()
    .populate({
      path: 'topic',
      select: 'tag -_id'
    })
    .then( result =>{
      console.log('GET ALL: ', result)
      res.status(200).json(result)
    }).catch(err =>{
      console.log('ERROR GET ALL: ', err)
      res.status(500).send(err)
    })
  }

  static create(req,res,next){
    console.log('REQ BODY: ', req.body)
    NewsModels.create({
      title: req.body.title,
      description: req.body.description,
      status: req.body.status,
      topic: req.body.topic
    }).then(result => {
      console.log('RESULT OF CREATE NEWS: ', result)
      res.status(201).send('succes create news: '+ req.body.title)
    }).catch( err => {
        console.log('ERROR CREATE NEWS: ', err)
        res.status(500).send('ERROR CREATE TOPICS')
    })
  }

  static filterStatus(req,res,next){
    NewsModels.find({status:{$in:req.query.status.split(",")}})
    .then(result => {
      console.log('RESULT OF FILTER STATUS NEWS: ', result)
      res.status(200).json(result)
    }).catch(err => {
      console.log('ERROR FILTER NEWS STATUS: ', err)
      res.status(500).send('ERROR FILTER NEWS STATUS')
    })
  }

  static filterTopic(req,res,next){
    NewsModels.find({topic:{$in:req.query.topic.split(",")}})
    .populate({
      path: 'topic',
      select: 'tag -_id',
    }).lean().then( result => {
      console.log('RESULT OF FILTER TOPIC: ', result)
      res.status(200).json(result)
    }).catch(err => {
      console.log('ERROR FILTER TOPIC', err)
      res.status(500).send('ERROR FILTER TOPIC', err)
    })
  }

  static update(req,res,next){
    NewsModels.findOneAndUpdate(
      {_id: req.params.newsId},
      {description: req.body.description},
      {new:true}
    ).then(result => {
      console.log('RESULT OF UPDATED NEWS: ', result)
      res.status(200).json(result)
    }).catch(err => {
      console.log('ERROR UPDATE NEWS: ', err)
      res.status(500).send(`Fail to update news data with Id ${req.params.newsId}`)
    })
  }
  static remove(req,res,next){
    NewsModels.findOneAndRemove({_id:req.params.newsId})
    .then( result => {
      console.log('DELETE RESULT SUCCESS: ', result)
      res.status(200).send(`data with Id ${req.params.newsId} has been deleted`)
    }).catch( err => {
      console.log('ERROR DELETE TOPICS TAG: ', err)
      res.status(500).send(`Fail to delete data with Id ${req.params.newsId}`)
    })
  }
}

module.exports = News
