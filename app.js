const express = require('express');
const app = express();
const bodyParser = require('body-parser')
const mongoose = require('mongoose');
const cors = require('cors'); //cross origins resource sharing
require('dotenv').config(); // env variable for development

app.use(cors()); //use this so can connect to client side
app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json())

let mongouri= "mongodb+srv://test-news-rest:test-news-rest@news-rest-api-hgeco.mongodb.net/test?retryWrites=true";
mongoose.connect(mongouri)
        .then(result =>{console.log('MONGO connected')})
        .catch(err =>{ console.log('ERROR, MONGO NOT CONNECTED')})

const basepath = '/api/kumparan/v1';
const topics   = require('./routes/topics');
const news     = require('./routes/news');

// routes
//topics
app.use(basepath+'/topics', topics)
//news
app.use(basepath+'/news', news)

app.get(basepath+'/bla',(req,res)=>{
  res.send('berhasil')
})

var port = 3000 || process.env.PORT
app.listen(port,err=>{
  if(err){console.log('Application not connected')}
  else{ console.log('Aplication Connect on PORT: '+port)}
})
